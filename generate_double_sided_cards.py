import os
import sys


def generate_double_sided_cards(file_path):
    base, ext = os.path.splitext(file_path)
    new_file_path = f"products/{base}.reversed{ext}"

    with open(file_path, "r") as fd:
        lines = fd.read().split("\n")

    with open(new_file_path, "w") as fd:
        for line in lines:
            split = line.split(",")
            if len(split) != 2:
                continue
            de, en = [word.strip() for word in split]
            fd.write(f"{de}, {en}\n")
            fd.write(f"{en}, {de}\n")


if __name__ == "__main__":
    generate_double_sided_cards(sys.argv[1])
