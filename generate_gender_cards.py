from typing import Iterable, Generator
import sys
import glob


def get_gender(line: str):
    mapping = {
        "der": "masculine",
        "das": "neuter",
        "die": "feminine"
    }
    word_with_article = line.split(", ")[0]
    split = word_with_article.split(" ")
    article = split[0].lower().strip()
    if article not in mapping:
        raise KeyError(f"looks like {line} isn't a noun")
    return article, " ".join(split[1:])


def get_gender_from_lines(lines: Iterable[str]) -> Generator[str, None, None]:
    for line in lines:
        try:
            yield get_gender(line)
        except Exception:
            continue


def generate_gender_cards(file_paths, output_file_path: str = "products/genders.txt"):
    lines = []
    for file_path in file_paths:
        with open(file_path, "r") as fd:
            lines.extend(fd.read().split("\n"))

    with open(output_file_path, "w") as fd:
        for article, word in get_gender_from_lines(lines):
            fd.write(f"{word}, {article}\n")


if __name__ == "__main__":

    if len(sys.argv) == 1:
        generate_gender_cards(list(glob.glob("*.txt")))
    else:
        generate_gender_cards([sys.argv[1]])
